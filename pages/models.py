from django.db import models
import datetime


# lista delle voci che vanno messe nei menu principali (home, riproduzioni, galleria, ecc)
class Menu(models.Model):
    name = models.CharField(max_length=20, unique=True) #indica il nome della categoria
    def __unicode__(self):
        return self.name


#filtra un'immagine su richiesta, i casi d'uso sono
# <img src="{{ object.image.url }}" alt="original image">
# <img src="{{ object.image|thumbnail }}" alt="image resized to default 104x104 format">
# <img src="{{ object.image|thumbnail:200x300 }}" alt="image resized to 200x300">
import os
import Image
from django.template import Library

# register = Library()

def thumbnail(file, size='104x104'):
    # defining the size
    x, y = [int(x) for x in size.split('x')]
    # defining the filename and the miniature filename
    filehead, filetail = os.path.split(file.path)
    basename, format = os.path.splitext(filetail)
    miniature = basename + '_' + size + format
    filename = file.path
    miniature_filename = os.path.join(filehead, miniature)
    filehead, filetail = os.path.split(file.url)
    miniature_url = filehead + '/' + miniature
    if os.path.exists(miniature_filename) and os.path.getmtime(filename)>os.path.getmtime(miniature_filename):
        os.unlink(miniature_filename)
    # if the image wasn't already resized, resize it
    if not os.path.exists(miniature_filename):
        image = Image.open(filename)
        image.thumbnail([x, y], Image.ANTIALIAS)
        try:
            image.save(miniature_filename, image.format, quality=90, optimize=1)
        except:
            image.save(miniature_filename, image.format, quality=90)

    return miniature_url

# register.filter(thumbnail)


# informazioni sull'immagine thumbnail
class Thumb(models.Model):
    image = models.ImageField(upload_to=models.settings.MEDIA_ROOT)
    def admin_image(self):
        return '<img src="%s" />' % self.image.url
    admin_image.allow_tags = True

    title = models.CharField(max_length=64, blank=True) # campo per non vedenti
    description = models.CharField(max_length=200, blank=True) # campo per non vedenti
    width = models.IntegerField(default=150, blank=True) # larghezza imposta
    height = models.IntegerField(default=150, blank=True) # altezza imposta
    visible = models.NullBooleanField(default=True)
    def __unicode__(self):
        return self.title


# informazioni sulla pagina
class Page(models.Model):
    page_menu = models.ForeignKey(Menu) # menu a cui appartiene la pagina
    title = models.CharField(max_length=50) # titolo della pagina cosi come comparira a video
    alias = models.CharField(max_length=50, unique=True) # titolo della pagina da inserire nell'url
    # alias = alias.replace(" ", "_")
    # page_items = models.ForeignKey(Item) #contenuto della pagina
    published = models.BooleanField(default=True) #flag che indica se la pagina e' pubblicata

    #metadati per SEO
    meta_description = models.CharField(max_length=200, blank=True) # meta descrizione
    meta_keyword = models.CharField(max_length=200, blank=True) #lista di keyword separate da virgola o punto-virgola

    def __unicode__(self):
        return self.title

    #informazioni esclusive per gli eventi
    # start_pub_date = models.DateField('Inizio Pubblicazione', default=datetime.date.today(), blank=True)
    # end_pub_date = models.DateField('Fine Pubblicazione', default=datetime.date.today(), blank=True)
    # archive = models.BooleanField(default=False) #quando un evento e' terminato, viene archiviato
    # poster_thumb = models.ForeignKey(Thumb, blank=True) #eventuale immagine dell'evento


# elementi di cui e' costituita la pagina
class Item(models.Model):
    item_page = models.ForeignKey(Page)
    title = models.CharField(max_length=100, blank=True)
    content = models.TextField(blank=True)
    published = models.BooleanField(default=True)
    pub_date = models.DateField('Data Pubblicazione', default=datetime.date.today()) # data di pubblicazione

    #TODO strumenti condivisione social network

    #campi per la pubblicazione della news
    news = models.BooleanField(default=True) #se questo flag e' true, la notizia viene pubblicata in prima pagina
    short_description = models.TextField(max_length=200, blank=True) #descrizione breve per la news nella home
    news_thumb = models.ForeignKey(Thumb, null=True, db_index=False, blank=True)# immagine da mostrare nella news

    def __unicode__(self):
        return self.title




#informazioni sugli eventi
# class Event(models.Model):
#     title = models.CharField(max_length=50) # titolo dell'evento
#     alias = models.CharField(max_length=50, blank=True, primary_key=True) # titolo dell'evento da inserire nell'url
#     content = models.TextField(blank=True) #contenuto dell'evento
#     published = models.BooleanField(default=True) #flag che indica se la pagina e' pubblicata
#     archive = models.BooleanField(default=False) #quando un evento e' terminato, viene archiviato
#     pub_date = models.DateField('Data Pubblicazione', default=datetime.date.today()) # data di pubblicazione
#     start_pub_date = models.DateField('Inizio Pubblicazione', default=datetime.date.today(), blank=True)
#     end_pub_date = models.DateField('Fine Pubblicazione', default=datetime.date.today(), blank=True)
#
#     #campi per la pubblicazione della news
#     news = models.BooleanField(default=True) #se questo flag e' true, l'evento viene pubblicato in prima pagina
#     short_description = models.TextField(max_length=200, blank=True) #descrizione breve per la news nella home
#     news_thumb = models.ForeignKey(Thumb, blank=True)# immagine da mostrare nella news
#
#     #metadati per SEO
#     meta_description = models.CharField(max_length=200, blank=True) # meta descrizione
#     meta_keyword = models.CharField(max_length=200, blank=True) #lista di keyword separate da virgola o punto-virgola



# Categoria         Pagina

# Riproduzioni |    Spade
#                   Lance
#                   Scudi
#                   Abbigliamento


# Bibliografia |    Celti
#                   Marche
#                   Mestieri

# Galleria     |    Museo Ancona
#                   Museo Milano
#                   British Museum


# Articoli     |    Tutorial accensione del fuoco
#                   Tutorial costruzione scudo
#                   Spada vichinga

# Home         |    Home
# Chi_sono          Chi sono
