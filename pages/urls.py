from django.conf.urls import patterns, include, url
from pages import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',

    url(r'(?P<page_name>[^/]+)/$', views.show_page), #riproduzioni, articoli, bibliografia

)