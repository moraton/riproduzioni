from django.contrib import admin

# Register your models here.
from pages.models import Page, Menu, Item, Thumb


class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'page_menu', 'published')


class ItemAdmin(admin.ModelAdmin):
    list_display = ('title', 'item_page', 'published', 'news', 'pub_date')

    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/js/tinymce_setup.js',
        ]

class ThumbAdmin(admin.ModelAdmin):
    list_display = ('title', 'image', 'width', 'height', 'visible')





admin.site.register(Menu)
admin.site.register(Page, PageAdmin)
admin.site.register(Thumb, ThumbAdmin)
admin.site.register(Item, ItemAdmin)

