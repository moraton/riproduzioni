from django.shortcuts import render
from django.http import HttpResponse
from pages.models import Page, Menu, Item
from django.core.exceptions import ObjectDoesNotExist

from photologue.models import Photo, Gallery


# visualizza la pagina
def show_page(request, page_name):
    home = False
    item_list = Item.objects.filter(item_page__alias=page_name, published=True)
    gallery_list = Gallery.objects.filter(is_public=True)
    #se la pagina e' una riproduzione
    if item_list.exists():
        context = {'home': home,
                   'item_list': item_list,
                   'page': Page.objects.get(alias=page_name),
                   'reprod_list': Page.objects.filter(page_menu__name="Riproduzioni"),
                   'article_list': Page.objects.filter(page_menu__name="Articoli"),
                   'gallery_exists': gallery_list.exists(),
                   'gallery_list': gallery_list,#Page.objects.filter(page_menu__name="Galleria"),
                   'biblio_list': Page.objects.filter(page_menu__name="Bibliografia")}
        return render(request, 'base.html', context)
    elif gallery_list.exists(): #TODO visualizzazione della galleria
        context = {'home': home,
                   'item_list': item_list,
                   'index_gallery': Gallery.objects.get(title_slug=page_name, is_public=True),
                   'reprod_list': Page.objects.filter(page_menu__name="Riproduzioni"),
                   'article_list': Page.objects.filter(page_menu__name="Articoli"),
                   'gallery_exists': gallery_list.exists(),
                   'gallery_list': gallery_list,#Page.objects.filter(page_menu__name="Galleria"),
                   'biblio_list': Page.objects.filter(page_menu__name="Bibliografia")}
        return render(request, 'base.html', context)
    else:
        return HttpResponse("Pagina non trovata in tabella") #TODO messaggio di errore pagina non trovata

