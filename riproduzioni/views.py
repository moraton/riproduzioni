from django.shortcuts import render
from django.http import HttpResponse
from pages.models import Page, Menu, Item
from django.core.exceptions import ObjectDoesNotExist

from photologue.models import Photo, Gallery


def show_home_page(request):
    try:
        Page.objects.get(alias='home')
    except ObjectDoesNotExist:
        menu1 = Menu.objects.create(name="Home")
        Page.objects.create(page_menu=menu1, title="Home", alias="home")
        menu2=Menu.objects.create(name="Chi sono")
        page2=Page.objects.create(page_menu=menu2, title="Chi sono", alias="chi_sono")
        Item.objects.create(item_page=page2, title="Chi sono", news=False)
        menu3=Menu.objects.create(name="Contatti")
        page3=Page.objects.create(page_menu=menu3, title="Contatti", alias="contatti")
        Item.objects.create(item_page=page3, title="Contatti", news=False)
        Menu.objects.create(name="Riproduzioni")
        Menu.objects.create(name="Articoli")
        Menu.objects.create(name="Galleria")
        Menu.objects.create(name="Bibliografia")

    gallery_list = Gallery.objects.filter(is_public=True)
    gallery_exists=False
    if (gallery_list.exists()):
        gallery_exists=True
    else:
        return HttpResponse("Galleria non esiste")

    home = True
    context = {'home': home,
               'page': Page.objects.get(alias='home'),
               'item_list': Item.objects.filter(news=True, published=True).order_by('pub_date'),
               'reprod_list': Page.objects.filter(page_menu__name="Riproduzioni"),
               'article_list': Page.objects.filter(page_menu__name="Articoli"),
               'gallery_exists': gallery_exists,
               'gallery_list': gallery_list,#Page.objects.filter(page_menu__name="Galleria"),
               'biblio_list': Page.objects.filter(page_menu__name="Bibliografia")}
    return render(request, 'base.html', context)
