from django.conf.urls import patterns, include, url
import views

from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.conf import settings


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',

    # (r'^photologue/', include('photologue.urls')),

    (r'^grappelli/', include('grappelli.urls')), # grappelli URLS

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^riproduzioni/', include('pages.urls')),
    url(r'^bibliografia/', include('pages.urls')),
    url(r'^articoli/', include('pages.urls')),

    # url(r'^galleria/', include(admin.site.urls)),

    # url(r'^eventi/', include('eventi.urls')),

    # url(r'(?P<page_name>[^/]+)/$', views.show_page),

    url(r'^$', views.show_home_page, name='home'),

    # url(r'^$', TemplateView.as_view(template_name="homepage.html"), name='homepage'),
) #+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
